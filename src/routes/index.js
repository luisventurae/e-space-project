'use strict'

const express = require('express')
const router = express.Router() 

// Public
router.get('/', (request, response) => {
    return response.render('home')
})

module.exports = router 
