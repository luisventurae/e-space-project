'use strict'

// const morgan = require('morgan')
const express = require('express')
const app = express()
const path = require('path')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const router = require ('./routes/index')
const session = require('express-session')
const flash = require('connect-flash')


const port = process.env.PORT || 3334

// Settings
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))

// Middlewares
// app.use(morgan('dev'))
app.use(express.static( path.join(__dirname, 'public') ))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(fileUpload())
app.use(flash())


// Routes
app.use(router)

// Starting Server
app. listen(port, () => {
    console.log(`Running on http://127.0.0.1:${port}`)
})
