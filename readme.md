# Application for Hackatrix 2019

To start to deploy the application on develoment mode

1. Install modules

```bash
npm install
```

2. Deploy to test

```bash
npm test
```

3. Deploy on production

```bash
npm start
```